var app = angular.module('myApp', []); 
app.controller('workoutController', function($scope) {

    $scope.list = [];//Variável para conter as atividades adicionadas
    $scope.add = 0;//Variável para contar a quantidade de itnes adicionados
    $scope.excluir = 0;//Variável para contar a quantidade de itnes excluídos
    $scope.count = 0;//Variável para contar a quantidade de itnes selecionados "checked"
    $scope.totalHours = 0;//Variável para somar a quantidade de horas de exercícios
    $scope.date = new Date();//Variável para identificar a data atual para o calendário

    $scope.exercises = [//Variável responsável por armazenar os nomes dos exercícios
        "Run",
        "Swimming",
        "Bike",
        "Dance",
        "Walking"
    ];

    $scope.addList = function(item) {//Função responsável por adicionar os itens da lista
        var len = $scope.list.length;//Identifica o tamanho da lista e cria os IDs
        $scope.list.push({//Insere os valores através de chave e valor
            id:         len,
            hours:      item.hoursList,
            data:       item.dataList,
            exercise:   item.timeSpent
        });
        $scope.add = $scope.add + 1;//Incrementa a variável de itens adcionados
        $scope.totalHours += item.hoursList;
        $scope.id = '';//Atribui a variável como vazia para uma futura instancia
        $scope.text = '';
        $scope.data = '';
    };

    $scope.remove = function(id, hours) {//Função responsável por remover os itens adicionados

        if(id != null){//Recebe o ID da lista como parâmetro para identificar qual item exatamente deve ser removido
            $scope.list.splice(id, 1);//Remove o item
            $scope.excluir = $scope.excluir + 1;//Incrementa a variável responsável por contar os itens excluídos
            $scope.totalHours = $scope.totalHours - hours;
        }
    };

    $scope.$watch('list', function(list){//Função responsável por assitir os itens com checkbox
        var selectedItems = 0;//Variável que verifica se o item possuío ou não o campo como checked
        angular.forEach(list, function(item){//Loop para correr todos os itens da lista
            selectedItems += item.selected ? 1 : 0;//Executa um intrução ternária, se o campo selected resultar em 1 incrementa o valor
        })
        $scope.count = selectedItems;//Incrementa a variável responsável por contar os intens selecionados
    },true);

});//Finaliza o escopo do controller do AngualarJS

$('.datepicker').pickadate({
    selectMonths: true, // Cria um dropdown para controlar o mês
    selectYears: 1, // Cria uma lista suspensa de 1 anos para controlar o ano
    format: 'dd/mm/yyyy' //Determina o formato da data
});